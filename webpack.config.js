const path = require('path');
const Dotenv = require('dotenv-webpack');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
   entry: "./src/server.ts",
   externals: [nodeExternals()],
   output: {
       filename: "bundle.js",
       path: path.resolve(__dirname, 'dist')
   },
   resolve: {
       extensions: [".webpack.js", ".web.js", ".ts", ".js"]
   },
   module: {
       rules: [{ test: /\.ts$/, loader: "ts-loader" }]
   },
   plugins: [
        new Dotenv({
            path: './.env',
            safe: false,
        }),
        new FilterWarningsPlugin({
            exclude: [/mongodb/, /mssql/, /mysql/, /oracledb/, /pg/, /pg-native/, /pg-query-stream/, /react-native-sqlite-storage/, /redis/, /sqlite3/, /sql.js/, /typeorm-aurora-data-api-driver/]
        })
   ],
   target: 'node',
   node: {
    global: false,
    __filename: 'mock',
    __dirname: 'mock',
  }
}