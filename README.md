# README - API for the Registration System for the Museum Run Association in Tuczno, Kujawsko-Pomorskie Voivodeship

## Introduction
This project is an API for a registration system developed as a freelance project for the Museum Run Association in Tuczno, Kujawsko-Pomorskie Voivodeship. It uses technologies such as Express, TypeScript, TypeORM, and MySQL to create an efficient and scalable API.

## Features
The API offers the following features:

- **Login and Registration**: Users can register and log in to the system.
- **Password Management**: Users can change and reset their passwords.
- **Event Creation**: Administrators can create events.
- **User Registration for Events**: Users can register for events.
- **Admin Panel**: An admin panel for managing users and events.

## Technologies Used 
- Express
- TypeScript
- TypeORM
- MySQL