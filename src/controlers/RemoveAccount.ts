import { getUser, getUserRepo } from './Utility';
import bcrypt from 'bcryptjs';
import { Enrollments } from '../entities/Enrollments';
export const removeAccount = async (req: any, res: any) => {
    const { email, password } = req.body;
    if (!email) return res.status(422).send({ 'error': "Brak wymaganego email'a!" })
    const User = await getUser(email);
    const dbHash = User ? User!.passwordHash : '';
    const isCorrectPass = await bcrypt.compare(password, dbHash);
    if (isCorrectPass) {
        res.status(200).send({ 'result': "Konto zostało usunięte!" })
        getUserRepo().delete(User!.id);
        await Enrollments.getRepository().delete({ email: User?.email });
    } else {
        res.status(422).send({ 'error': "Hasło jest niepoprawne!" })
    }
}
