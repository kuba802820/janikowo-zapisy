import { Events } from "../entities/Events";
import { Enrollments } from "../entities/Enrollments";

export const GetEvents = async (req: any, res: any) => {
    try {
        const events = await Events.find();
        const email = req.session.data.email;
        const UserAllEnrollments = await Enrollments.find({ where: { email } });
        const eventIds: number[] = [];
        UserAllEnrollments.forEach((el) => {
            eventIds.push(el.eventId);
        })
        return res.status(200).send({ "result": { 'eventsData': events, "alreadyEnrolments": eventIds } });

    } catch  {
        return res.status(500).send({ 'result': 'Coś poszło nie tak!' });
    }
}