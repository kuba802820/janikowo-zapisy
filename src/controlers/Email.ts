import nodemailer from 'nodemailer';
const hbs = require('nodemailer-express-handlebars');
import dotenv from 'dotenv';
dotenv.config();
let transporter = nodemailer.createTransport({
	host: `${process.env.SMTP_HOST}`,
	port: Number(process.env.SMTP_PORT),
	secure: process.env.SMTP_SECURE === "true" ? true : false,
	auth: {
		user: process.env.SMTP_USER,
		pass: process.env.SMTP_PASSWORD
	}
});
const options = {
	viewEngine: {
		extname: '.hbs',
		layoutsDir: './views/email/',
		defaultLayout: 'template',
		partialsDir: './views/partials/'
	},
	viewPath: './views/email/',
	extName: '.hbs'
};


transporter.use('compile', hbs(options));
export const sendMail = async (email: string, token: string, name: string, redirectHost?: string, ) => {
	console.log("wysłano")
	const mail = {
		from: 'muzealnytuczno@muzealnytuczno.pl',
		to: `${email}`,
		subject: 'Reset Hasła - <noreply-zapisy.muzealnytuczno.pl>',
		template: 'email',
		context: {
			token,
			redirectHost,
			name,
		}
	}
	await transporter.sendMail(mail);
	transporter.close();
}