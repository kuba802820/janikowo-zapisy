import { Results } from '../entities/Results';
import { validationResult } from 'express-validator';
import { Enrollments } from '../entities/Enrollments';

export const GetResults = async (req: any, res: any) => {
    const { firstName, lastName } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    const result = await Results.findOne({ where: { firstName, lastName } });
    res.status(200).json(result);
}

export const GetEnrolmentsForUsers = async (req: any, res: any) => {
    const { eventId } = req.body;
    const usersAssigned = await Enrollments.find({ select: ["firstName", "lastName", "city", "id"], where: { eventId } });
    res.status(200).json({ "result": usersAssigned });
}

export const GetEnrolmentsForAdmin = async (req: any, res: any) => {
    const { eventId } = req.query;
    const result = await Enrollments.find({ where: { eventId } });
    res.status(200).json({ "result": result });
}