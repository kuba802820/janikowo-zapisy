import { generateHash } from './Utility';
import { Users } from '../entities/Users';
import { validationResult } from 'express-validator';
const insertUser = async (res: any, data: any) => {
    try {
        const User = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            passwordHash: data.passwordHash,
            telNumber: data.telNumber,
            city: data.city,
            club: data.club,
            gender: data.gender,
            role: 'MEMBER',
            dateBrith: data.dateBrith,
            shirtSize: data.shirtSize
        }
        await Users.createQueryBuilder().insert().into(Users).values(User).execute();
        res.status(200).send({ 'result': 'Założono konto, możesz się zalogować' })
    } catch (error) {
        res.status(500).send({ 'error': 'Błąd podczas zakładania konta!' })
    }
}

export const validateAndRegisterUser = async (req: any, res: any) => {
    const { email, password } = req.body;
    const isExistEmail = await Users.findOne({ where: { email } });
    const result = validationResult(req);
    const passwordHash = await generateHash(password);
    if (isExistEmail) return res.status(422).send({ 'error': 'Konto o podanym emailu już istnieje!' });
    if (!result.isEmpty()) return res.status(422).json({ errors: result.array() });
    await insertUser(res, { ...req.body, passwordHash });
}