import { isLogin } from './Utility';
import { validationResult, Result, ValidationError } from 'express-validator';
import { Enrollments } from '../entities/Enrollments';
import { Events } from '../entities/Events';

type shirtSize = 'S' | 'M' | 'L' | 'XL' | 'XXL';

type gender = 'K' | 'M' | 'Inne';

interface IEnrollments {
    firstName?: string,
    lastName?: string,
    email?: string,
    dateBrith?: string,
    city?: string,
    telNumber?: number,
    club?: string,
    shirtSize?: shirtSize,
    gender?: gender,
    assignBy?: string
    eventId?: number,
}

interface IsetDataPerson {
    assignOtherPerson: string,
    assignBy: { assignBy: string },
    req: any,
}

interface IvalidateEnrolment {
    isUserEnroll: Enrollments | undefined,
    isIdExist: Events | undefined,
    res: any,
    req: any,
    errors: Result<ValidationError>,
    assignBy: { assignBy: string },
    assignOtherPerson: string
}

const setDataPerson = ({ assignOtherPerson, req, assignBy }: IsetDataPerson) => {
    let dataPerson: IEnrollments = {};
    if (isLogin(req.session)) assignOtherPerson === 'false'
        ? dataPerson = { ...req.session.data, ...req.body, ...assignBy }
        : dataPerson = { ...req.body, ...assignBy };
    return dataPerson;
}

const validateEnrolment = ({ isUserEnroll, isIdExist, res, req, errors, assignBy, assignOtherPerson }: IvalidateEnrolment) => {
    if (isUserEnroll) {
        res.status(422).send({ 'error': 'Taki użytkownik jest już zapisany na wydarzenie!' })
        return false;
    }
    if (!isIdExist) {
        res.status(422).send({ 'error': 'Nie ma wydarzenia o podanym ID' });
        return false;
    }
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    const data = setDataPerson({ assignOtherPerson, req, assignBy });
    return data;
}

export const AssignEvent = async (req: any, res: any) => {
    try {
        const { assignOtherPerson, email, eventId } = req.body;
        const assignBy = { assignBy: `${req.session.data.firstName} ${req.session.data.lastName}` };
        const errors = validationResult(req);
        const EmailByAssignOtherPerson = assignOtherPerson === 'false' ? req.session.data.email : email
        const isUserEnroll = await Enrollments.findOne({ where: { email: EmailByAssignOtherPerson, eventId } });
        const isIdExist = await Events.findOne(eventId);
        if (isIdExist.isActive) {
            const data = validateEnrolment({ isUserEnroll, isIdExist, req, res, errors, assignBy, assignOtherPerson })
            if (data) {
                Events.createQueryBuilder().update(Events).set({ joinedUser: () => 'joinedUser + 1' }).where('id = :id', { id: eventId }).execute()
                Enrollments.createQueryBuilder().insert().into(Enrollments).values(data).execute();
                return res.status(200).send({ 'result': 'Pomyślnie zapisano!', });
            }
        } else {
            return res.status(422).send({ 'result': 'Zapisy zostały zablokowane!', });
        }

    } catch  {
        return res.status(500).send({ 'result': 'Coś poszło nie tak!' });
    }
}
