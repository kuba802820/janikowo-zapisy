import jwt from 'jsonwebtoken';
import { Users } from '../entities/Users';
import { validationResult } from 'express-validator';
import { generateHash, isLogin, getUser } from './Utility';
import { sendMail } from './Email';
import crypto from 'crypto';
import bcrypt from 'bcryptjs';

const UpdatePasswordInDB = async (newPassword: string, idUser: number) => {
  const hash = await generateHash(newPassword);
  Users.createQueryBuilder().update(Users).set({ passwordHash: hash }).where({ id: idUser }).execute();
}

const validatePasswordAndUpdate = async (token: string, { res, req }: any, { newPassword, confirmPassword }: any) => {
  if (!token) return res.status(400).send({ 'error': 'Brak tokena!' });
  try {
    const errors = validationResult(req);
    const { user }: any = jwt.decode(token);
    const idUser = user.id;
    await UpdatePasswordInDB(newPassword, idUser);
    if (!jwt.verify(token, `${process.env.SECRET_KEY}`)) return res.status(401).send({ 'error': 'Błąd walidacji Tokena!' });
    if (!crypto.timingSafeEqual(Buffer.from(newPassword), (Buffer.from(confirmPassword)))) return res.status(422).send({ 'error': 'Podane hasła nie są identyczne!' });
    if (!errors.isEmpty()) return res.status(422).send({ 'result': { errors: errors.array() } });
    return res.status(200).send({ 'result': 'Hasło zostało zmienione, możesz się zalogować' });
  } catch (error) {
    return res.status(500).send({ 'error': 'Coś poszło nie tak!' });
  }
}


const validatePasswordAndUpdateWithoutToken = async ({ res, req }: any, { actualPassword, newPassword, confirmPassword }: any) => {
  try {
    const errors = validationResult(req);
    const User = await getUser(req.session.data.email);
    const idUser = User!.id;
    const actualPasswordHash = await bcrypt.compare(actualPassword, User!.passwordHash);
    if (!errors.isEmpty()) return res.status(422).send({ 'result': { errors: errors.array() } });
    if (!actualPasswordHash) return res.status(422).send({ 'error': 'Aktualne hasło jest nieprawidłowe!' });
    if (!isLogin(req.session)) return res.status(422).send({ 'error': 'Musisz się zalogować!' });
    if (!crypto.timingSafeEqual(Buffer.from(newPassword), (Buffer.from(confirmPassword)))) return res.status(422).send({ 'error': 'Podane hasła nie są identyczne!' });
    await UpdatePasswordInDB(newPassword, idUser);
    res.status(200).send({ 'result': 'Hasło zostało zmienione' });
  } catch (error) {
    res.status(500).send({ 'error': "Coś poszło nie tak!" });
  }
}

export const resetPassword = async (req: any, res: any) => {
  const { actualPassword, newPassword, confirmPassword } = req.body;
  const token = String(req.header('Authorization')).replace('Bearer ', '');
  (token !== 'null')
    ? await validatePasswordAndUpdate(token, { req, res }, { newPassword, confirmPassword })
    : await validatePasswordAndUpdateWithoutToken({ req, res }, { actualPassword, newPassword, confirmPassword })
}



export const assignTokenAndSendMail = async (req: any, res: any) => {
  const { email } = req.body;
  const User = await Users.findOne({ where: { email } });
  if (User) {
    const resetToken = jwt.sign({ user: User }, `${process.env.SECRET_KEY}`, { expiresIn: '4min' });
    await sendMail(email, resetToken, User.firstName, process.env.CLIENT_RESET_PAGE_ADDRESS);
  }
  res.status(200).send({ 'result': 'true' });
}