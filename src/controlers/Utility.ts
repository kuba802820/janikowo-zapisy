import { getRepository } from 'typeorm';
import { Users } from '../entities/Users';
import bcrypt from 'bcryptjs';

export const isLogin = (session: Express.Session) => session.data ? true : false;

export const getUserRepo = () => getRepository(Users);

export const getUser = (email: string) => getUserRepo().findOne({ where: { email } });

export const getLoginUser = (session: Express.Session) => { if (session.data) return session.data; }

export const generateHash = async (value: string) => {
    const salt = await bcrypt.genSalt();
    return bcrypt.hash(value, salt);
}