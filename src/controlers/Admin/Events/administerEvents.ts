import { Events } from "../../../entities/Events";
import { validationResult } from "express-validator";
import { Enrollments } from "../../../entities/Enrollments";

export const createEvent = async (req: any, res: any) => {
    const { eventName, rulesLink }: { eventName: string, rulesLink: string } = req.body;
    if (!eventName && !rulesLink) return res.status(422).send({ "error": 'Nie podano wymaganych parametrów' })
    const isExistEvent = await Events.findOne({ where: { eventName } })
    isExistEvent ? res.status(422).send({ "error": 'Wydarzenie o podanej nazwie już istnieje!' }) : await Events.createQueryBuilder().insert().into(Events).values({
        eventName,
        rulesLink,
        joinedUser: 0,
    }).execute();
    return res.status(200).send({ "result": 'Wydarzenie zostało dodane!' });
}

const checkId = (id: number, req: any, res: any) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });
    if (!id) return res.status(422).send({ 'error': 'Brak wymaganego id' })
}

export const deleteEvent = async (req: any, res: any) => {
    const { id }: { id: number } = req.query;
    checkId(id, req, res);
    const isEventInDb = await Events.findOne({ where: { id } });
    if (!isEventInDb) return res.status(422).send({ 'error': 'Nie znaleziono wydarzenia o podanym id' })
    await Events.delete(id);
    await Enrollments.delete({
        eventId: id
    })
    return res.status(200).send({ "result": 'Wydarzenie zostało usunięte!' })
}
export const UserPay = async (req: any, res: any) => {
    const { id, isPay }: { id: number, isPay: number } = req.query;
    checkId(id, req, res);
    const isUserInDb = await Enrollments.findOne({ where: { id } });
    if (!isUserInDb) return res.status(422).send({ 'error': 'Nie znaleziono użytkownika o podanym id' })
    await Enrollments.createQueryBuilder().update(Enrollments).set({ isPay }).where({ id }).execute();
    return res.status(200).send({ "result": `Użytkownik ${isUserInDb.firstName} ${isUserInDb.lastName} został pomyślnie odznaczony!` })
}
export const ToggleEventsEnrolments = async (req: any, res: any) => {
    const { id, isActive }: { id: number, isActive: number } = req.query;
    checkId(id, req, res);
    const isEventInDb = await Events.findOne({ where: { id } });
    if (!isEventInDb) return res.status(422).send({ 'error': 'Nie znaleziono wydarzenia o podanym id' })
    await Events.createQueryBuilder().update(Events).set({ isActive }).where({ id }).execute();
    return res.status(200).send({ "result": `Pomyślnie zmieniono stan wydarzenia!` })
}