import { getUser, isLogin } from './Utility';
import bcrypt from 'bcryptjs';

interface ILogin {
  req: Express.Request,
  res: any,
  email: string,
  password: string,
}

export const Login = async ({ req, res, email, password }: ILogin) => {
  try {
    const User = await getUser(email);
    const dbHash = User ? User!.passwordHash : '';
    const isCorrectPass = await bcrypt.compare(password, dbHash);
    if (isLogin(req.session!)) return res.status(422).send({ 'error': 'Jesteś już zalogowany!' });
    if (!User || !isCorrectPass && email.length || password.length > 20) return res.status(422).send({ 'error': 'Niepoprawny email lub hasło!' });
    req.session!.data = {
      firstName: User?.firstName,
      lastName: User?.lastName,
      email: User?.email,
      role: User?.role,
      dateBrith: User?.dateBrith,
      city: User?.city,
      telNumber: User?.telNumber,
      club: User?.club,
      shirtSize: User?.shirtSize,
      gender: User?.gender,
      createdAt: User?.createdAt,
    };
    const LoginReturn = { 'result': true, email, userId: req.sessionID, role: User?.role, club: User?.club }
    return res.status(200).send({ "result": LoginReturn });
  } catch (error) {
    return res.status(500).send({ 'error': 'Coś poszło nie tak!' });
  }
}

export const Logout = (session: Express.Session, res: any) => {
  if (session.data) session.destroy((err) => err)
  res.status(200).send({ result: true });
}