import {Router} from 'express';
import register from '../routes/register';
import login from '../routes/login';
import logout from '../routes/logout';
import me from '../routes/me';
import sendresetemail from '../routes/sendresetemail';
import resetpassword from '../routes/resetpassword';
import assigntoevent from '../routes/assigntoevent';
import getresult from '../routes/getresult';
import allresult from '../routes/allresult';
export const routerApi = () => {
        return Router().use('/register', register)
        .use('/login', login)
        .use('/me', me)
        .use('/logout', logout)
        .use('/resetpassword', resetpassword)
        .use('/sendresetemail', sendresetemail)
        .use('/assigntoevent', assigntoevent)
        .use('/getresult', getresult)
        .use('/allresult', allresult);
}
