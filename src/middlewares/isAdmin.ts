export default (req:any, res:any, next: () => void) => {
    req.session.data.role === 'ADMIN' ? next() : res.status(401).send({'error':'Nie masz uprawnień!'})
}