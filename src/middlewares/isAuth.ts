import { isLogin } from "../controlers/Utility";
export default (req:any, res:any, next: () => void) => {
    !isLogin(req.session) ? res.status(401).send({'error':'Musisz się zalogować!'}) : next();
}