import isAuth from "./isAuth";

import isAdmin from "./isAdmin";

import { check } from "express-validator";

export const adminMiddleware = () => [
        isAuth, 
        isAdmin,
        check('id').isLength({min:1}).withMessage("Id jest wymagane!").escape().isNumeric(),
]