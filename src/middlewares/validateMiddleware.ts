/* eslint-disable security/detect-unsafe-regex */
import { ValidationChain, check, body } from "express-validator";

export const validationMiddleware: ValidationChain[] = [
    check('email').isEmail().withMessage('Podany adres email nie jest poprawny!').exists().normalizeEmail().escape(),
    check('dateBrith').isISO8601().withMessage('Podana data urodzenia jest nie prawidłowa!').escape(),
    body(['email', 'firstName', 'lastName', 'city', 'club', 'Sex', 'dateBrith', 'shirtSize']).isLength({ min: 1 }).withMessage('Nie wszystkie pola są uzupełnione!').escape(),
    check('password').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć min 8 znaków oraz musi zawierać jedną dużą literę, oraz cyfrę').exists().escape(),
]