import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn } from "typeorm";
@Entity()
export class Events extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column() eventName: string;

    @Column() rulesLink: string;

    @Column() joinedUser: number;

    @Column() isActive: number;

    @CreateDateColumn() createdAt: string;


}