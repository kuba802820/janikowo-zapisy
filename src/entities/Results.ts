import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
@Entity()
export class Results extends BaseEntity {
    @PrimaryGeneratedColumn() id: number;

    @Column() firstName: string;

    @Column() lastName: string;

    @Column() startNumber: number;

    @Column() nettoResult: number;

    @Column() bruttoResult: number;

    @Column() place: number;

    @Column() ageCategory: number;

    @Column() placeInAgeCategory: number;

    @Column() eventName: string;

}