import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn } from 'typeorm';
@Entity()
export class Users extends BaseEntity {
    @PrimaryGeneratedColumn() id: number;

    @Column() firstName: string;

    @Column() lastName: string;

    @Column() email: string;

    @Column() passwordHash: string;

    @Column() role: string;

    @Column() dateBrith: string;

    @Column() city: string;

    @Column() telNumber: number;

    @Column() club: string;

    @Column() shirtSize: string;

    @Column() gender: string;

    @CreateDateColumn() createdAt: string;

    @UpdateDateColumn({ type: 'timestamp' }) updatedAt: number;

}
