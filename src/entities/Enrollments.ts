import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn } from "typeorm";
@Entity()

export class Enrollments extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column() firstName: string;

    @Column() lastName: string;

    @Column() email: string;

    @Column() dateBrith: string;

    @Column() city: string;

    @Column() telNumber: number;

    @Column() club: string;

    @Column() shirtSize: string;

    @Column() gender: string;

    @Column() eventId: number;

    @Column() isPay: number;

    @Column() assignBy: string;

    @CreateDateColumn() assignDate: string;
}