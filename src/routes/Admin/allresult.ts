import express from 'express';
import isAuth from '../../middlewares/isAuth';
import isAdmin from '../../middlewares/isAdmin';
import { GetEnrolmentsForAdmin } from '../../controlers/UserResults';
const router = express.Router();
router.get('', [isAuth, isAdmin], GetEnrolmentsForAdmin);
export default router;
