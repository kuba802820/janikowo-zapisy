import express from 'express';
import { UserPay } from '../../controlers/Admin/Events/administerEvents';
import isAdmin from '../../middlewares/isAdmin';
const router = express.Router();

router.get('', isAdmin, UserPay);
export default router;
