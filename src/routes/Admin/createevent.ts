import express from 'express';
import isAuth from '../../middlewares/isAuth';
import isAdmin from '../../middlewares/isAdmin';
import { createEvent } from '../../controlers/Admin/Events/administerEvents';
const router = express.Router();

router.post('', [isAuth, isAdmin],createEvent);
export default router;
