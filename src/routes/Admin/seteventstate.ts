import express from 'express';
import { ToggleEventsEnrolments } from '../../controlers/Admin/Events/administerEvents';
import isAdmin from '../../middlewares/isAdmin';
const router = express.Router();

router.get('', isAdmin, ToggleEventsEnrolments);
export default router;
