import express from 'express';
import { deleteEvent } from '../../controlers/Admin/Events/administerEvents';
import isAdmin from '../../middlewares/isAdmin';
const router = express.Router();
router.get('', isAdmin, deleteEvent);
export default router;
