import express from 'express';
import isAuth from '../middlewares/isAuth';
const router = express.Router();

router.get('', isAuth, (req, res) => {
    res.status(200).send({ 'result': req.session!.data });
});

export default router;
