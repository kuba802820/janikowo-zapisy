import express from 'express';
import { check, body } from 'express-validator';
import { validateAndRegisterUser } from '../controlers/Register';
const router = express.Router();

router.post('', [
    check('email').isEmail().withMessage('Podany adres email nie jest poprawny!').exists().normalizeEmail().escape().isLength({ max: 128 }),
    check('dateBrith').isISO8601().withMessage('Podana data urodzenia jest nie prawidłowa!').escape().isLength({ max: 128 }),
    check('telNumber').matches(/^([0-9]{3})([0-9]{3})([0-9]{3})$/).withMessage('Podany numer telefonu nie jest poprawny!').escape(),
    body(['email', 'firstName', 'lastName', 'telNumber', 'city', 'gender', 'dateBrith', 'shirtSize']).isLength({ min: 1 }).withMessage('Nie wszystkie pola są uzupełnione!').escape().isLength({ max: 128 }),
    check('password').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć długość od 8-15 znaków, musi zawierać jedną dużą literę, oraz cyfrę').exists().escape().isLength({ max: 128 }),
], validateAndRegisterUser);
export default router;
