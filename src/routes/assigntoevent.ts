import express from 'express';
import isAuth from '../middlewares/isAuth';
import { AssignEvent } from '../controlers/AssignEvent';
const router = express.Router();
router.post('', [isAuth], AssignEvent);
export default router;
