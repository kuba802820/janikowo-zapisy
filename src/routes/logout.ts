import express from 'express';
import isAuth from '../middlewares/isAuth';
import { Logout } from '../controlers/Auth';
const router = express.Router();
router.post('', isAuth, (req: any, res: any) => Logout(req.session,res));
export default router;
