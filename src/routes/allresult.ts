import express from 'express';
import isAuth from '../middlewares/isAuth';
import { GetEnrolmentsForUsers } from '../controlers/UserResults';
const router = express.Router();
router.post('', [isAuth], GetEnrolmentsForUsers);
export default router;
