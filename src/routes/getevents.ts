import express from 'express';
import isAuth from '../middlewares/isAuth';
import { GetEvents } from '../controlers/GetEvents';
const router = express.Router();
router.post('', [isAuth], GetEvents);
export default router;
