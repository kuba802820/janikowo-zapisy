import express from 'express';
import { check } from 'express-validator';
import { resetPassword } from '../controlers/Reset';
const router = express.Router();
router.post('', [
    check('newPassword').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć długość min. 8 znaków, musi zawierać jedną dużą literę, oraz cyfrę').exists().escape(),
], [resetPassword]);

export default router;
