import express from 'express';
import isAuth from '../middlewares/isAuth';
import { check } from 'express-validator';
import { GetResults } from '../controlers/UserResults';
const router = express.Router();
router.post('', [
    isAuth,
    check('firstName').isLength({min:1}).withMessage('Imie jest wymagane!').escape(),
    check('lastName').isLength({min:1}).withMessage('Nazwisko jest wymagane!').escape(),
], GetResults);
export default router;
