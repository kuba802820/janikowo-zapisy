import express from 'express';
import { removeAccount } from '../controlers/RemoveAccount';
import isAuth from '../middlewares/isAuth';
const router = express.Router();
router.post('', isAuth, removeAccount);
export default router;
