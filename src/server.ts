import 'reflect-metadata';

import dotenv from 'dotenv';
import express from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';
import rateLimit from 'express-rate-limit';
import path from 'path';
import helmet from 'helmet';
import { createConnection } from 'typeorm';
import https from 'https';
import http from 'http';
import fs from 'fs';
import register from './routes/register';
import login from './routes/login';
import logout from './routes/logout';
import me from './routes/me';
import sendresetemail from './routes/sendresetemail';
import resetpassword from './routes/resetpassword';
import assigntoevent from './routes/assigntoevent';
import removeAccount from './routes/removeaccount';
import getresult from './routes/getresult';
import allresult from './routes/allresult';
import getevents from './routes/getevents';
import { default as adminallresult } from './routes/Admin/allresult';
import createevent from './routes/Admin/createevent';
import markpay from './routes/Admin/markpay';
import seteventstate from './routes/Admin/seteventstate';
import removeevent from './routes/Admin/removeevent';

import { Users } from './entities/Users';
import { Results } from './entities/Results';
import { Events } from './entities/Events';
import { Enrollments } from './entities/Enrollments';


dotenv.config();
const FileStore = require('session-file-store')(session); //TODO
const app = express();
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true, parameterLimit: 10000 }));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'views'));
const apiLimiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100,
  message: 'Too many requests'
});

app.use('/api', apiLimiter);

app.use(session({
  name: 'session',
  secret: String(process.env.SECRET_KEY),
  resave: false,
  store: new FileStore(),
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7, //7Days
    httpOnly: true,
    sameSite: false,
    secure: false
  }
}));

app.use((_req, res, next) => {
  res.header('Content-Type', 'application/json;charset=UTF-8')
  res.header('Access-Control-Allow-Credentials', "true")
  res.header('Access-Control-Allow-Origin', `${process.env.CLIENT_ORIGIN}`);
  res.header('Access-Control-Allow-Methods', 'POST, GET');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  next();
});

app.use('/api/register', register)
  .use('/api/login', login)
  .use('/api/me', me)
  .use('/api/logout', logout)
  .use('/api/resetpassword', resetpassword)
  .use('/api/sendresetemail', sendresetemail)
  .use('/api/assigntoevent', assigntoevent)
  .use('/api/getresult', getresult)
  .use('/api/allresult', allresult)
  .use('/api/getevents', getevents)
  .use('/api/removeaccount', removeAccount)
  .use('/api/admin/allResult', adminallresult)
  .use('/api/admin/createEvent', createevent)
  .use('/api/admin/removeEvent', removeevent)
  .use('/api/admin/markPay', markpay)
  .use('/api/admin/setEventState', seteventstate);
app.use(helmet());

const connectToDb = async () => {

  await createConnection({
    name: "default",
    type: "mysql",
    host: process.env.DB_HOST_NAME,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: true,
    logging: true,
    "entities": [
      Users,
      Events,
      Results,
      Enrollments,
    ]
  });

}
const httpServer = http.createServer(app);
if (process.env.NODE_ENV === 'production') {
  const privateKey = fs.readFileSync('/etc/letsencrypt/live/muzealnytuczno.pl/privkey.pem', 'utf8');
  const certificate = fs.readFileSync('/etc/letsencrypt/live/muzealnytuczno.pl/cert.pem', 'utf8');
  const ca = fs.readFileSync('/etc/letsencrypt/live/muzealnytuczno.pl/chain.pem', 'utf8');
  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
  };
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(Number(process.env.HTTPS_SERVER_PORT), async () => {
    await connectToDb();
    console.log(`Server HTTPS ready at: http://localhost:${process.env.HTTPS_SERVER_PORT}`);
  });

}

httpServer.listen(Number(process.env.HTTP_SERVER_PORT), async () => {
  await connectToDb();
  console.log(`Server HTTP ready at: http://localhost:${process.env.HTTP_SERVER_PORT}`);
});


