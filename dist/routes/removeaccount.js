"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var RemoveAccount_1 = require("../controlers/RemoveAccount");
var isAuth_1 = __importDefault(require("../middlewares/isAuth"));
var router = express_1.default.Router();
router.post('', isAuth_1.default, RemoveAccount_1.removeAccount);
exports.default = router;
//# sourceMappingURL=removeaccount.js.map