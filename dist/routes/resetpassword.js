"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_validator_1 = require("express-validator");
var Reset_1 = require("../controlers/Reset");
var router = express_1.default.Router();
router.post('', [
    express_validator_1.check('newPassword').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć długość min. 8 znaków, musi zawierać jedną dużą literę, oraz cyfrę').exists().escape(),
], [Reset_1.resetPassword]);
exports.default = router;
//# sourceMappingURL=resetpassword.js.map