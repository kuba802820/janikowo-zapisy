"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var isAuth_1 = __importDefault(require("../../middlewares/isAuth"));
var isAdmin_1 = __importDefault(require("../../middlewares/isAdmin"));
var administerEvents_1 = require("../../controlers/Admin/Events/administerEvents");
var router = express_1.default.Router();
router.post('', [isAuth_1.default, isAdmin_1.default], administerEvents_1.createEvent);
exports.default = router;
//# sourceMappingURL=createevent.js.map