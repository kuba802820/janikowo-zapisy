"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var administerEvents_1 = require("../../controlers/Admin/Events/administerEvents");
var isAdmin_1 = __importDefault(require("../../middlewares/isAdmin"));
var router = express_1.default.Router();
router.get('', isAdmin_1.default, administerEvents_1.deleteEvent);
exports.default = router;
//# sourceMappingURL=removeevent.js.map