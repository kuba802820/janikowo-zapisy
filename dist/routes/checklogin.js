"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var CheckLogin_1 = require("../controlers/CheckLogin");
var router = express_1.default.Router();
router.get('', CheckLogin_1.CheckLogin);
exports.default = router;
//# sourceMappingURL=checklogin.js.map