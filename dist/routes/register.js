"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var express_validator_1 = require("express-validator");
var Register_1 = require("../controlers/Register");
var router = express_1.default.Router();
router.post('', [
    express_validator_1.check('email').isEmail().withMessage('Podany adres email nie jest poprawny!').exists().normalizeEmail().escape().isLength({ max: 128 }),
    express_validator_1.check('dateBrith').isISO8601().withMessage('Podana data urodzenia jest nie prawidłowa!').escape().isLength({ max: 128 }),
    express_validator_1.check('telNumber').matches(/^([0-9]{3})([0-9]{3})([0-9]{3})$/).withMessage('Podany numer telefonu nie jest poprawny!').escape(),
    express_validator_1.body(['email', 'firstName', 'lastName', 'telNumber', 'city', 'gender', 'dateBrith', 'shirtSize']).isLength({ min: 1 }).withMessage('Nie wszystkie pola są uzupełnione!').escape().isLength({ max: 128 }),
    express_validator_1.check('password').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć długość od 8-15 znaków, musi zawierać jedną dużą literę, oraz cyfrę').exists().escape().isLength({ max: 128 }),
], Register_1.validateAndRegisterUser);
exports.default = router;
//# sourceMappingURL=register.js.map