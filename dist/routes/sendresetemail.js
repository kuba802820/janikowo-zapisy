"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var Reset_1 = require("../controlers/Reset");
var router = express_1.default.Router();
router.post('', Reset_1.assignTokenAndSendMail);
exports.default = router;
//# sourceMappingURL=sendresetemail.js.map