"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var isAuth_1 = __importDefault(require("../middlewares/isAuth"));
var express_validator_1 = require("express-validator");
var UserResults_1 = require("../controlers/UserResults");
var router = express_1.default.Router();
router.post('', [
    isAuth_1.default,
    express_validator_1.check('firstName').isLength({ min: 1 }).withMessage('Imie jest wymagane!').escape(),
    express_validator_1.check('lastName').isLength({ min: 1 }).withMessage('Nazwisko jest wymagane!').escape(),
], UserResults_1.GetResults);
exports.default = router;
//# sourceMappingURL=getresult.js.map