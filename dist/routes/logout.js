"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var isAuth_1 = __importDefault(require("../middlewares/isAuth"));
var Auth_1 = require("../controlers/Auth");
var router = express_1.default.Router();
router.post('', isAuth_1.default, function (req, res) { return Auth_1.Logout(req.session, res); });
exports.default = router;
//# sourceMappingURL=logout.js.map