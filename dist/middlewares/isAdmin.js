"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (req, res, next) {
    req.session.data.role === 'ADMIN' ? next() : res.status(401).send({ 'error': 'Nie masz uprawnień!' });
});
//# sourceMappingURL=isAdmin.js.map