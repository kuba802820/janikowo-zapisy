"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable security/detect-unsafe-regex */
var express_validator_1 = require("express-validator");
exports.validationMiddleware = [
    express_validator_1.check('email').isEmail().withMessage('Podany adres email nie jest poprawny!').exists().normalizeEmail().escape(),
    express_validator_1.check('dateBrith').isISO8601().withMessage('Podana data urodzenia jest nie prawidłowa!').escape(),
    express_validator_1.body(['email', 'firstName', 'lastName', 'city', 'club', 'Sex', 'dateBrith', 'shirtSize']).isLength({ min: 1 }).withMessage('Nie wszystkie pola są uzupełnione!').escape(),
    express_validator_1.check('password').isLength({ min: 8 }).matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/).withMessage('Hasło musi mieć min 8 znaków oraz musi zawierać jedną dużą literę, oraz cyfrę').exists().escape(),
];
//# sourceMappingURL=validateMiddleware.js.map