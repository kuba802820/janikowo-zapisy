"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utility_1 = require("../controlers/Utility");
exports.default = (function (req, res, next) {
    !Utility_1.isLogin(req.session) ? res.status(401).send({ 'error': 'Musisz się zalogować!' }) : next();
});
//# sourceMappingURL=isAuth.js.map