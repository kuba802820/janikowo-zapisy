"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var isAuth_1 = __importDefault(require("./isAuth"));
var isAdmin_1 = __importDefault(require("./isAdmin"));
var express_validator_1 = require("express-validator");
exports.adminMiddleware = function () { return [
    isAuth_1.default,
    isAdmin_1.default,
    express_validator_1.check('id').isLength({ min: 1 }).withMessage("Id jest wymagane!").escape().isNumeric(),
]; };
//# sourceMappingURL=idAdmin.js.map