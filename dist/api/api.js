"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var register_1 = __importDefault(require("../routes/register"));
var login_1 = __importDefault(require("../routes/login"));
var logout_1 = __importDefault(require("../routes/logout"));
var me_1 = __importDefault(require("../routes/me"));
var sendresetemail_1 = __importDefault(require("../routes/sendresetemail"));
var resetpassword_1 = __importDefault(require("../routes/resetpassword"));
var assigntoevent_1 = __importDefault(require("../routes/assigntoevent"));
var getresult_1 = __importDefault(require("../routes/getresult"));
var allresult_1 = __importDefault(require("../routes/allresult"));
exports.routerApi = function () {
    return express_1.Router().use('/register', register_1.default)
        .use('/login', login_1.default)
        .use('/me', me_1.default)
        .use('/logout', logout_1.default)
        .use('/resetpassword', resetpassword_1.default)
        .use('/sendresetemail', sendresetemail_1.default)
        .use('/assigntoevent', assigntoevent_1.default)
        .use('/getresult', getresult_1.default)
        .use('/allresult', allresult_1.default);
};
//# sourceMappingURL=api.js.map