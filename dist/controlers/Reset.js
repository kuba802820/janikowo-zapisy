"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var Users_1 = require("../entities/Users");
var express_validator_1 = require("express-validator");
var Utility_1 = require("./Utility");
var Email_1 = require("./Email");
var crypto_1 = __importDefault(require("crypto"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var UpdatePasswordInDB = function (newPassword, idUser) { return __awaiter(void 0, void 0, void 0, function () {
    var hash;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, Utility_1.generateHash(newPassword)];
            case 1:
                hash = _a.sent();
                Users_1.Users.createQueryBuilder().update(Users_1.Users).set({ passwordHash: hash }).where({ id: idUser }).execute();
                return [2 /*return*/];
        }
    });
}); };
var validatePasswordAndUpdate = function (token, _a, _b) {
    var res = _a.res, req = _a.req;
    var newPassword = _b.newPassword, confirmPassword = _b.confirmPassword;
    return __awaiter(void 0, void 0, void 0, function () {
        var errors, user, idUser, error_1;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    if (!token)
                        res.status(400).send({ 'error': 'Brak tokena!' });
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 3, , 4]);
                    errors = express_validator_1.validationResult(req);
                    user = jsonwebtoken_1.default.decode(token).user;
                    idUser = user.id;
                    return [4 /*yield*/, UpdatePasswordInDB(newPassword, idUser)];
                case 2:
                    _c.sent();
                    res.status(200).send({ 'result': 'Hasło zostało zmienione, możesz się zalogować' });
                    if (!jsonwebtoken_1.default.verify(token, "" + process.env.SECRET_KEY))
                        res.status(401).send({ 'error': 'Błąd walidacji Tokena!' });
                    if (!crypto_1.default.timingSafeEqual(Buffer.from(newPassword), (Buffer.from(confirmPassword))))
                        res.status(422).send({ 'error': 'Podane hasła nie są identyczne!' });
                    if (!errors.isEmpty())
                        res.status(422).send({ 'result': { errors: errors.array() } });
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _c.sent();
                    res.status(500).send({ 'error': 'Coś poszło nie tak!' });
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    });
};
var validatePasswordAndUpdateWithoutToken = function (_a, _b) {
    var res = _a.res, req = _a.req;
    var actualPassword = _b.actualPassword, newPassword = _b.newPassword, confirmPassword = _b.confirmPassword;
    return __awaiter(void 0, void 0, void 0, function () {
        var errors, User, idUser, actualPasswordHash, error_2;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    _c.trys.push([0, 4, , 5]);
                    errors = express_validator_1.validationResult(req);
                    return [4 /*yield*/, Utility_1.getUser(req.session.data.email)];
                case 1:
                    User = _c.sent();
                    idUser = User.id;
                    return [4 /*yield*/, bcryptjs_1.default.compare(actualPassword, User.passwordHash)];
                case 2:
                    actualPasswordHash = _c.sent();
                    if (!errors.isEmpty())
                        return [2 /*return*/, res.status(422).send({ 'result': { errors: errors.array() } })];
                    if (!actualPasswordHash)
                        return [2 /*return*/, res.status(422).send({ 'error': 'Aktualne hasło jest nieprawidłowe!' })];
                    if (!Utility_1.isLogin(req.session))
                        return [2 /*return*/, res.status(422).send({ 'error': 'Musisz się zalogować!' })];
                    if (!crypto_1.default.timingSafeEqual(Buffer.from(newPassword), (Buffer.from(confirmPassword))))
                        return [2 /*return*/, res.status(422).send({ 'error': 'Podane hasła nie są identyczne!' })];
                    return [4 /*yield*/, UpdatePasswordInDB(newPassword, idUser)];
                case 3:
                    _c.sent();
                    res.status(200).send({ 'result': 'Hasło zostało zmienione' });
                    return [3 /*break*/, 5];
                case 4:
                    error_2 = _c.sent();
                    res.status(500).send({ 'error': "Coś poszło nie tak!" });
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    });
};
exports.resetPassword = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, actualPassword, newPassword, confirmPassword, token, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _a = req.body, actualPassword = _a.actualPassword, newPassword = _a.newPassword, confirmPassword = _a.confirmPassword;
                token = String(req.header('Authorization')).replace('Bearer ', '');
                if (!(token !== 'null')) return [3 /*break*/, 2];
                return [4 /*yield*/, validatePasswordAndUpdate(token, { req: req, res: res }, { newPassword: newPassword, confirmPassword: confirmPassword })];
            case 1:
                _b = _c.sent();
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, validatePasswordAndUpdateWithoutToken({ req: req, res: res }, { actualPassword: actualPassword, newPassword: newPassword, confirmPassword: confirmPassword })];
            case 3:
                _b = _c.sent();
                _c.label = 4;
            case 4:
                _b;
                return [2 /*return*/];
        }
    });
}); };
exports.assignTokenAndSendMail = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var email, User, resetToken;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                email = req.body.email;
                return [4 /*yield*/, Users_1.Users.findOne({ where: { email: email } })];
            case 1:
                User = _a.sent();
                if (!User) return [3 /*break*/, 3];
                resetToken = jsonwebtoken_1.default.sign({ user: User }, "" + process.env.SECRET_KEY, { expiresIn: '4min' });
                return [4 /*yield*/, Email_1.sendMail(email, resetToken, User.firstName, process.env.CLIENT_RESET_PAGE_ADDRESS)];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3:
                res.status(200).send({ 'result': 'true' });
                return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=Reset.js.map