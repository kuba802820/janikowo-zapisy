"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Utility_1 = require("./Utility");
var express_validator_1 = require("express-validator");
var Enrollments_1 = require("../entities/Enrollments");
var Events_1 = require("../entities/Events");
var setDataPerson = function (_a) {
    var assignOtherPerson = _a.assignOtherPerson, req = _a.req, assignBy = _a.assignBy;
    var dataPerson = {};
    if (Utility_1.isLogin(req.session))
        assignOtherPerson === 'false'
            ? dataPerson = __assign(__assign(__assign({}, req.session.data), req.body), assignBy)
            : dataPerson = __assign(__assign({}, req.body), assignBy);
    return dataPerson;
};
var validateEnrolment = function (_a) {
    var isUserEnroll = _a.isUserEnroll, isIdExist = _a.isIdExist, res = _a.res, req = _a.req, errors = _a.errors, assignBy = _a.assignBy, assignOtherPerson = _a.assignOtherPerson;
    if (isUserEnroll) {
        res.status(422).send({ 'error': 'Taki użytkownik jest już zapisany na wydarzenie!' });
        return false;
    }
    if (!isIdExist) {
        res.status(422).send({ 'error': 'Nie ma wydarzenia o podanym ID' });
        return false;
    }
    if (!errors.isEmpty())
        return res.status(422).json({ errors: errors.array() });
    var data = setDataPerson({ assignOtherPerson: assignOtherPerson, req: req, assignBy: assignBy });
    return data;
};
exports.AssignEvent = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, assignOtherPerson, email, eventId, assignBy, errors, EmailByAssignOtherPerson, isUserEnroll, isIdExist, data, _b;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 3, , 4]);
                _a = req.body, assignOtherPerson = _a.assignOtherPerson, email = _a.email, eventId = _a.eventId;
                assignBy = { assignBy: req.session.data.firstName + " " + req.session.data.lastName };
                errors = express_validator_1.validationResult(req);
                EmailByAssignOtherPerson = assignOtherPerson === 'false' ? req.session.data.email : email;
                return [4 /*yield*/, Enrollments_1.Enrollments.findOne({ where: { email: EmailByAssignOtherPerson, eventId: eventId } })];
            case 1:
                isUserEnroll = _c.sent();
                return [4 /*yield*/, Events_1.Events.findOne(eventId)];
            case 2:
                isIdExist = _c.sent();
                data = validateEnrolment({ isUserEnroll: isUserEnroll, isIdExist: isIdExist, req: req, res: res, errors: errors, assignBy: assignBy, assignOtherPerson: assignOtherPerson });
                if (data) {
                    Events_1.Events.createQueryBuilder().update(Events_1.Events).set({ joinedUser: function () { return 'joinedUser + 1'; } }).where('id = :id', { id: eventId }).execute();
                    Enrollments_1.Enrollments.createQueryBuilder().insert().into(Enrollments_1.Enrollments).values(data).execute();
                    return [2 /*return*/, res.status(200).send({ 'result': 'Pomyślnie zapisano!', })];
                }
                return [3 /*break*/, 4];
            case 3:
                _b = _c.sent();
                return [2 /*return*/, res.status(500).send({ 'result': 'Coś poszło nie tak!' })];
            case 4: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=AssignEvent.js.map