"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var dotenv_1 = __importDefault(require("dotenv"));
var express_1 = __importDefault(require("express"));
var express_session_1 = __importDefault(require("express-session"));
var body_parser_1 = __importDefault(require("body-parser"));
var express_rate_limit_1 = __importDefault(require("express-rate-limit"));
var path_1 = __importDefault(require("path"));
var helmet_1 = __importDefault(require("helmet"));
var typeorm_1 = require("typeorm");
var register_1 = __importDefault(require("./routes/register"));
var login_1 = __importDefault(require("./routes/login"));
var logout_1 = __importDefault(require("./routes/logout"));
var me_1 = __importDefault(require("./routes/me"));
var sendresetemail_1 = __importDefault(require("./routes/sendresetemail"));
var resetpassword_1 = __importDefault(require("./routes/resetpassword"));
var assigntoevent_1 = __importDefault(require("./routes/assigntoevent"));
var removeaccount_1 = __importDefault(require("./routes/removeaccount"));
var getresult_1 = __importDefault(require("./routes/getresult"));
var allresult_1 = __importDefault(require("./routes/allresult"));
var getevents_1 = __importDefault(require("./routes/getevents"));
var allresult_2 = __importDefault(require("./routes/Admin/allresult"));
var createevent_1 = __importDefault(require("./routes/Admin/createevent"));
var markpay_1 = __importDefault(require("./routes/Admin/markpay"));
var removeevent_1 = __importDefault(require("./routes/Admin/removeevent"));
var Users_1 = require("./entities/Users");
var Results_1 = require("./entities/Results");
var Events_1 = require("./entities/Events");
var Enrollments_1 = require("./entities/Enrollments");
dotenv_1.default.config();
var FileStore = require('session-file-store')(express_session_1.default); //TODO
var app = express_1.default();
app.use(body_parser_1.default.json({ limit: '5mb' }));
app.use(body_parser_1.default.urlencoded({ limit: '5mb', extended: true, parameterLimit: 10000 }));
app.set('view engine', 'hbs');
app.set('views', path_1.default.join(__dirname, 'views'));
var apiLimiter = express_rate_limit_1.default({
    windowMs: 15 * 60 * 1000,
    max: 100,
    message: 'Too many requests'
});
app.use('/api', apiLimiter);
app.use(express_session_1.default({
    name: 'session',
    secret: String(process.env.SECRET_KEY),
    resave: false,
    store: new FileStore(),
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7,
        httpOnly: true,
        sameSite: false,
        secure: false
    }
}));
app.use(function (_req, res, next) {
    res.header('Content-Type', 'application/json;charset=UTF-8');
    res.header('Access-Control-Allow-Credentials', "true");
    res.header('Access-Control-Allow-Origin', "" + process.env.CLIENT_ORIGIN);
    res.header('Access-Control-Allow-Methods', 'POST, GET');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});
app.use('/api/register', register_1.default)
    .use('/api/login', login_1.default)
    .use('/api/me', me_1.default)
    .use('/api/logout', logout_1.default)
    .use('/api/resetpassword', resetpassword_1.default)
    .use('/api/sendresetemail', sendresetemail_1.default)
    .use('/api/assigntoevent', assigntoevent_1.default)
    .use('/api/getresult', getresult_1.default)
    .use('/api/allresult', allresult_1.default)
    .use('/api/getevents', getevents_1.default)
    .use('/api/removeaccount', removeaccount_1.default)
    .use('/api/admin/allResult', allresult_2.default)
    .use('/api/admin/createEvent', createevent_1.default)
    .use('/api/admin/removeEvent', removeevent_1.default)
    .use('/api/admin/markPay', markpay_1.default);
app.use(helmet_1.default());
app.listen(Number(process.env.HTTP_SERVER_PORT), function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, typeorm_1.createConnection({
                    name: "default",
                    type: "mysql",
                    host: process.env.DB_HOST_NAME,
                    port: Number(process.env.DB_PORT),
                    username: process.env.DB_USERNAME,
                    password: process.env.DB_PASSWORD,
                    database: process.env.DB_NAME,
                    synchronize: true,
                    logging: true,
                    "entities": [
                        Users_1.Users,
                        Events_1.Events,
                        Results_1.Results,
                        Enrollments_1.Enrollments,
                    ]
                })];
            case 1:
                _a.sent();
                console.log("Server ready at: http://localhost:" + process.env.HTTP_SERVER_PORT);
                return [2 /*return*/];
        }
    });
}); });
//# sourceMappingURL=server.js.map